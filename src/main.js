import Vue from 'vue'
import VueRouter from 'vue-router';
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import GameMenu from './views/GameMenu'
import GamePlay from './views/GamePlay'
import GameOver from './views/GameOver'

Vue.use(BootstrapVue)
Vue.use(VueRouter);
Vue.config.productionTip = false

const router = new VueRouter({
  base: '/',
  mode: 'history',
  routes: [
    { path: '/', component: GameMenu },
    { path: '/trivia', component: GamePlay},
    { path: '/gameover', name: 'gameover', component: GameOver}
  ]})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')



